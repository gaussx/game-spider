# Game Spider

The purpose of Game Spider is to provide you with fast price comparisons from a wide range of game (and game key) vendors.

# Dependencies

If you want to host Game Spider yourself, you need a system which has Chromedriver in PATH and the Selenium Python module.