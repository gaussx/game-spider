from selenium import webdriver
import bs4
import re
from datetime import datetime
from math import ceil

links = {"steam": "https://store.steampowered.com/search/?term=", "epic": "https://store.epicgames.com/en-US/browse?q="}

def find_game_info(website: str, mode: str, info: str):
    game_info = {"title": 'NaN', "price": "-1", "discount": '-0%'}
    # searchtime = datetime.now().timestamp()
    unsatisfied = 1
    current_index = 0
    while unsatisfied:
        if info == "all" or info == "title":
            try:
                if mode == "steam":
                    game_info["title"] = website.find_all(class_="title")[current_index].get_text()
                elif mode == "epic":
                    game_info["title"] = website.find_all(class_="css-rk7ym5")[current_index].get_text()
            except:
                print("Error occured. Aborting data haul.")
        if info == "all" or info == "price":
            try:
                if mode == "steam":
                    game_info["price"] = website.find_all(class_="col search_price_discount_combined responsive_secondrow")[current_index].get_text().strip()
                    potential_data = game_info["price"].split()
                    if len(potential_data) > 1:
                        game_info["discount"] = potential_data[0]
                        game_info["price"] = potential_data[1][ceil(len(potential_data[1]) / 2):]
                elif mode == "epic":
                    relevant_data = game_info["title"].split()[-2:]
                    game_info["price"] = relevant_data[1] + relevant_data[0][-3:]
            except:
                print("Error occured. Aborting data haul.")
        current_index += 1
        if len(game_info["price"]) > 0 and type(game_info["price"]) != type(int):
            unsatisfied = 0
    # searchtime = datetime.now().timestamp() - searchtime
    # print("Found the title and price in", searchtime)
    return game_info

spider = webdriver.Chrome()
spider_options = webdriver.chrome.options.Options()
spider_options.add_argument("--headless")
spider.headless = True

#game = input("Enter desired game name: ")
game = "gta 5"

for source in links.keys():
    # exetime = datetime.now().timestamp()
    spider.get(links[source] + game)
    website = bs4.BeautifulSoup(spider.page_source.encode("utf-8"), 'lxml')
    if source == "epic":
        a = open("strona_epic.html", 'w') # this is something i'm using for data analysis ignore
        a.write(str(website))             # ignore those two lines
    game_info = find_game_info(website, source, "all")
    # exetime = datetime.now().timestamp() - exetime
    # print("Data fetched from {SOURCE} in {TIME}s".format(SOURCE = source, TIME = exetime))
    if source == "steam":
        game = game_info["title"]
        print("Game title:", game_info["title"])
    print("Price on {SOURCE}: {PRICE}".format(SOURCE = source.capitalize(), PRICE = game_info["price"]), end = ' ')
    if game_info["discount"] != '-0%':
        print("(currently on sale: {DISCOUNT})".format(DISCOUNT = game_info["discount"]), end = '')
    print()


spider.quit()
